﻿using Dicom.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Dicom.Controllers
{
    public class UploadController : ApiController
    {
        // GET api/upload
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/upload/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/upload
        public void Post([FromBody]string value)
        {
        }

        // PUT api/upload/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/upload/5
        public void Delete(int id)
        {
        }

        // Upload api/upload/5
        public void Upload(int id)
        {
            //server open port
        }

        /// <summary>
        /// Load the Elements in the DCM Meta 
        /// </summary>
        /// <param name="dcm">the DICOM file</param>
        private void LoadAndSaveDCMMetaInfo(DicomFile dcm)
        {
            //Show the message of DCM file to the listview in the dialog.
            DicomDataset dcmDataset = dcm.Dataset;
            DicomFileMetaInformation dcmMetaInfo = dcm.FileMetaInfo;
            //listView1.BeginUpdate();
            //DICOM MetaInfo
            if (!String.IsNullOrWhiteSpace(dcmMetaInfo.MediaStorageSOPClassUID == null ? "" : dcmMetaInfo.MediaStorageSOPClassUID.ToString()))
            {
                //Insert the ImplementationClassUID
                //ListViewItem newItem = new ListViewItem("0002");
                //newItem.SubItems.Add("0002");
                //newItem.SubItems.Add(dcmMetaInfo.MediaStorageSOPClassUID.ToString());
                //listView1.Items.Add(newItem);
            }
            if (!String.IsNullOrWhiteSpace(dcmMetaInfo.MediaStorageSOPInstanceUID == null ? "" : dcmMetaInfo.MediaStorageSOPInstanceUID.ToString()))
            {
                //Insert the ImplementationClassUID
                //ListViewItem newItem = new ListViewItem("0002");
                //newItem.SubItems.Add("0003");
                //newItem.SubItems.Add(dcmMetaInfo.MediaStorageSOPInstanceUID.ToString());
                //listView1.Items.Add(newItem);

            }
            if (!String.IsNullOrWhiteSpace(dcmMetaInfo.TransferSyntax == null ? "" : dcmMetaInfo.TransferSyntax.ToString()))
            {
                //Insert the ImplementationClassUID
                //ListViewItem newItem = new ListViewItem("0002");
                //newItem.SubItems.Add("0010");
                //newItem.SubItems.Add(dcmMetaInfo.TransferSyntax.ToString());
                //listView1.Items.Add(newItem);

            }
            if (!String.IsNullOrWhiteSpace(dcmMetaInfo.ImplementationClassUID == null ? "" : dcmMetaInfo.ImplementationClassUID.ToString()))
            {
                //Insert the ImplementationClassUID
                //ListViewItem newItem = new ListViewItem("0002");
                //newItem.SubItems.Add("0012");
                //newItem.SubItems.Add(dcmMetaInfo.ImplementationClassUID.ToString());
                //listView1.Items.Add(newItem);

            }
            if (!String.IsNullOrWhiteSpace(dcmMetaInfo.ImplementationVersionName == null ? "" : dcmMetaInfo.ImplementationVersionName.ToString()))
            {
                //Insert the ImplementationClassUID
                //ListViewItem newItem = new ListViewItem("0002");
                //newItem.SubItems.Add("0013");
                //newItem.SubItems.Add(dcmMetaInfo.ImplementationVersionName.ToString());
                //listView1.Items.Add(newItem);

            }

            //Patient Info
            Patient patient = new Patient();
            Study study = new Study();
            Series series = new Series();

            string patientName = dcmDataset.Get<string>(DicomTag.PatientName, "");
            if (!String.IsNullOrWhiteSpace(patientName))
            {
                //ListViewItem newItem = new ListViewItem("0010");
                //newItem.SubItems.Add("0010");
                //newItem.SubItems.Add(patientName);
                //listView1.Items.Add(newItem);
                patient.Name = patientName;
            }
            string patientID = dcmDataset.Get<string>(DicomTag.PatientID, "");
            if (!String.IsNullOrWhiteSpace(patientID))
            {
                //ListViewItem newItem = new ListViewItem("0010");
                //newItem.SubItems.Add("0020");
                //newItem.SubItems.Add(patientID);
                //listView1.Items.Add(newItem);
                patient.PatientId = patientID;
            }
            string patientBirth = dcmDataset.Get<string>(DicomTag.PatientBirthDate);
            if (!String.IsNullOrWhiteSpace(patientBirth))
            {
                //ListViewItem newItem = new ListViewItem("0010");
                //newItem.SubItems.Add("0030");
                //newItem.SubItems.Add(patientBirth);
                //listView1.Items.Add(newItem);
                patient.DOB = patientBirth;
            }
            string patientSex = dcmDataset.Get<string>(DicomTag.PatientSex);
            if (!String.IsNullOrWhiteSpace(patientSex))
            {
                //ListViewItem newItem = new ListViewItem("0010");
                //newItem.SubItems.Add("0040");
                //newItem.SubItems.Add(patientSex);
                //listView1.Items.Add(newItem);
                patient.Gender = patientSex;
            }
            string patientAge = dcmDataset.Get<string>(DicomTag.PatientAge);
            if (!String.IsNullOrWhiteSpace(patientAge))
            {
                //ListViewItem newItem = new ListViewItem("0010");
                //newItem.SubItems.Add("1010");
                //newItem.SubItems.Add(patientAge);
                //listView1.Items.Add(newItem);
                patient.Age = Convert.ToInt16(patientAge);
            }
            //Save patient info to database


            //Study & Series Info
            string studyDate = dcmDataset.Get<string>(DicomTag.StudyDate);
            if (!String.IsNullOrWhiteSpace(studyDate))
            {
                //ListViewItem newItem = new ListViewItem("0008");
                //newItem.SubItems.Add("0020");
                //newItem.SubItems.Add(studyDate);
                //listView1.Items.Add(newItem);
                study.StudyDate = studyDate;
            }
            string studyTime = dcmDataset.Get<string>(DicomTag.StudyTime);
            if (!String.IsNullOrWhiteSpace(studyTime))
            {
                //ListViewItem newItem = new ListViewItem("0008");
                //newItem.SubItems.Add("0030");
                //newItem.SubItems.Add(studyTime);
                //listView1.Items.Add(newItem);
                study.StudyTime = studyTime;
            }
            string studyInstanceID = dcmDataset.Get<string>(DicomTag.StudyInstanceUID);
            if (!String.IsNullOrWhiteSpace(studyInstanceID))
            {
                //ListViewItem newItem = new ListViewItem("0020");
                //newItem.SubItems.Add("000D");
                //newItem.SubItems.Add(studyInstanceID);
                //listView1.Items.Add(newItem);
                study.StudyId = studyInstanceID;
            }
            string seriesInstanceID = dcmDataset.Get<string>(DicomTag.SeriesInstanceUID);
            if (!String.IsNullOrWhiteSpace(seriesInstanceID))
            {
                //ListViewItem newItem = new ListViewItem("0020");
                //newItem.SubItems.Add("000E");
                //newItem.SubItems.Add(seriesInstanceID);
                //listView1.Items.Add(newItem);
                series.Id = seriesInstanceID;
            }
            string studyID = dcmDataset.Get<string>(DicomTag.StudyID);
            if (!String.IsNullOrWhiteSpace(studyID))
            {
                //ListViewItem newItem = new ListViewItem("0020");
                //newItem.SubItems.Add("0010");
                //newItem.SubItems.Add(studyID);
                //listView1.Items.Add(newItem);
            }
            //save study info to database
            //save series info to database



            //Image Info
            //string imageHeight = dcmDataset.Get<string>(DicomTag.Rows);
            //if (!String.IsNullOrWhiteSpace(imageHeight))
            //{
            //    ListViewItem newItem = new ListViewItem("0028");
            //    newItem.SubItems.Add("0010");
            //    newItem.SubItems.Add(imageHeight);
            //    listView1.Items.Add(newItem);
            //}
            //string imageWidth = dcmDataset.Get<string>(DicomTag.Columns);
            //if (!String.IsNullOrWhiteSpace(imageWidth))
            //{
            //    ListViewItem newItem = new ListViewItem("0028");
            //    newItem.SubItems.Add("0011");
            //    newItem.SubItems.Add(imageWidth);
            //    listView1.Items.Add(newItem);
            //}
            //string bitsAlloctaed = dcmDataset.Get<string>(DicomTag.BitsAllocated);
            //if (!String.IsNullOrWhiteSpace(bitsAlloctaed))
            //{
            //    ListViewItem newItem = new ListViewItem("0028");
            //    newItem.SubItems.Add("0100");
            //    newItem.SubItems.Add(bitsAlloctaed);
            //    listView1.Items.Add(newItem);
            //}
            //string bitsStored = dcmDataset.Get<string>(DicomTag.BitsStored);
            //if (!String.IsNullOrWhiteSpace(bitsStored))
            //{
            //    ListViewItem newItem = new ListViewItem("0028");
            //    newItem.SubItems.Add("0101");
            //    newItem.SubItems.Add(bitsStored);
            //    listView1.Items.Add(newItem);
            //}
            //string highBits = dcmDataset.Get<string>(DicomTag.HighBit);
            //if (!String.IsNullOrWhiteSpace(highBits))
            //{
            //    ListViewItem newItem = new ListViewItem("0028");
            //    newItem.SubItems.Add("0102");
            //    newItem.SubItems.Add(highBits);
            //    listView1.Items.Add(newItem);
            //}

            //listView1.EndUpdate();

        }

    }
}

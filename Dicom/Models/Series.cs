﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dicom.Models
{
    public class Series
    {
        public string Id { get; set; }
        public string Modality { get; set; }
        public string SeriesNumber { get; set; }
        public string Description { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Dicom.Models
{
    public class Configuration
    {
        public int Port { get; set; }
        public string StorageFolder { get; set; }
    }
}
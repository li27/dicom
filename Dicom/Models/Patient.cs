﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Dicom.Models
{
    public class Patient
    {
        [Display(Name = "Patient Id")]
        public string PatientId { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Gender")]
        public string Gender { get; set; }

        [Display(Name = "Date of Birth")]
        public string DOB { get; set; }

        [Display(Name = "Age")]
        public int Age { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Dicom.Models
{
    public class Study
    {
        [Display(Name = "Study Id")]
        public string StudyId { get; set; } 

        [Display(Name = "Patient Id")]
        public string PatientId { get; set; }

        [Display(Name = "Study Description")]
        public string Description { get; set; }

        [Display(Name = "Accession Number")]
        public string AccessionNumber { get; set; }
        //public int Age { get; set; }

        [Display(Name = "Study Date")]
        public string StudyDate { get; set; }

        [Display(Name = "Study Time")]
        public string StudyTime { get; set; }
    }
}